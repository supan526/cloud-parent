package com.gome.cloud.service;

import com.gome.cloud.entity.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Author supan
 * @Date 2017/6/9 16:55
 * @Copyright(c) gome inc Gome Co.,LTD
 */
//- 使用@FeignClient("xxx")注解来绑定该接口对应xxxx服务
@FeignClient(value = "service-provider",fallback = FeignConsumerHystrix.class)
public interface FeignConsumerClient {

    @GetMapping(value = "/add")
    Integer add(@RequestParam(value = "a") Integer a,@RequestParam(value = "b") Integer b);

    @RequestMapping(value = "/user/{id}")
    User findById(@PathVariable("id") Long id);

    @GetMapping("/users")
    List<User> findAll();
}

