package com.gome.cloud.service;

import com.gome.cloud.entity.User;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author supan
 * @Date 2017/6/9 16:56
 * @Copyright(c) gome inc Gome Co.,LTD
 */
@Component
public class FeignConsumerHystrix implements FeignConsumerClient {
    @Override
    public Integer add(@RequestParam(value = "a") Integer a, @RequestParam(value = "b") Integer b) {
        return -9999;
    }

    @Override
    public User findById(Long id) {
        User user = new User();
        user.setId(-1L);
        user.setUsername("default username");
        user.setAge(0);
        return user;
    }

    @Override
    public List<User> findAll() {
        User user = new User();
        user.setId(-1L);
        user.setUsername("default username");
        user.setAge(0);
        List<User> users = new ArrayList<>();
        users.add(user);
        return users;
    }
}
