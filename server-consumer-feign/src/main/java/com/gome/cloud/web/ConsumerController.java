package com.gome.cloud.web;

import com.gome.cloud.entity.User;
import com.gome.cloud.service.FeignConsumerClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author supan
 * @Date 2017/6/9 16:54
 * @Copyright(c) gome inc Gome Co.,LTD
 */
@RestController
public class ConsumerController {

    @Autowired
    private FeignConsumerClient feignConsumerClient;

    @GetMapping(value = "/add")
    public Integer add(){
        return feignConsumerClient.add(12,23);
    }

    @GetMapping(value = "/user/{id}")
    public User findById(@PathVariable("id") Long id){
        return feignConsumerClient.findById(id);
    }

    @GetMapping("/users")
    public List<User> findAll(){
        return feignConsumerClient.findAll();
    }


}
