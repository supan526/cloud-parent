package com.gome.cloud.entity;

import org.springframework.data.annotation.Id;

import java.util.Date;

/**
 * @Author supan
 * @Date 2017/7/3 9:28
 * @Copyright(c) gome inc Gome Co.,LTD
 */
public class Event {
    @Id
    private String id;
    private Integer numberLimit;
    private String mainPhoto;
    private String desc;
    private Date startAt;
    private Date endAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getNumberLimit() {
        return numberLimit;
    }

    public void setNumberLimit(Integer numberLimit) {
        this.numberLimit = numberLimit;
    }

    public String getMainPhoto() {
        return mainPhoto;
    }

    public void setMainPhoto(String mainPhoto) {
        this.mainPhoto = mainPhoto;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Date getStartAt() {
        return startAt;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

    public Date getEndAt() {
        return endAt;
    }

    public void setEndAt(Date endAt) {
        this.endAt = endAt;
    }
}
