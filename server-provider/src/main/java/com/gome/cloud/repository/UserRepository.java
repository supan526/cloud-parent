package com.gome.cloud.repository;

import com.gome.cloud.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author supan
 * @Date 2017/7/3 9:30
 * @Copyright(c) gome inc Gome Co.,LTD
 */
@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    User findByUsername(String username);

}
