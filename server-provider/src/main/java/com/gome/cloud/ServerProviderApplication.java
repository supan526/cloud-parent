package com.gome.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient //激活 Eureka中的 DiscoveryClient 实现
public class ServerProviderApplication {
	public static void main(String[] args) {
		SpringApplication.run(ServerProviderApplication.class, args);
	}
}
