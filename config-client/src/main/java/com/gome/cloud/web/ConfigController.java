package com.gome.cloud.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author supan
 * @Date 2017/6/26 16:52
 * @Copyright(c) gome inc Gome Co.,LTD
 */
@RefreshScope // @RefreshScope注解不能少，否则即使调用/refresh，配置也不会刷新
@RestController
public class ConfigController {
    @Value("${from}")
    private String from;
    @RequestMapping("/from")
    public String from(){
        return from;
    }





}
