package com.gome.cloud.web;

import com.gome.cloud.entity.User;
import com.gome.cloud.service.RibbonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author supan
 * @Date 2017/6/9 17:45
 * @Copyright(c) gome inc Gome Co.,LTD
 */
@RestController()
public class RibbonConsumerController {

    @Autowired
    public RibbonService ribbonService;
    @Autowired
    public DiscoveryClient discoveryClient;

    @GetMapping(value = "/add")
    public String add(){
        return ribbonService.add();
    }

    @GetMapping("/users")
    public List<User> findAll() {
        return ribbonService.findAll();
    }
    @GetMapping("/user/{id}")
    public User findUserById(@PathVariable  Long id){
        return  ribbonService.findUserById(id);
    }
    /**
     * 本地服务实例的信息
     * @return
     */
    @GetMapping("/info")
    public ServiceInstance showInfo() {
        ServiceInstance localServiceInstance = this.discoveryClient.getLocalServiceInstance();
        return localServiceInstance;
    }

}
