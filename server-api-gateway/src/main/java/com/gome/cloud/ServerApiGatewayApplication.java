package com.gome.cloud;

import com.gome.cloud.filter.AccessFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

@SpringCloudApplication
@EnableZuulProxy  //使用@EnableZuulProxy注解开启Zuul的API网关服务功能。
public class ServerApiGatewayApplication {
	@Bean
	public AccessFilter accessFilter(){
		return new AccessFilter();
	}

	public static void main(String[] args) {
		SpringApplication.run(ServerApiGatewayApplication.class, args);
	}
}
